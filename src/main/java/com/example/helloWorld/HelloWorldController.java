package com.example.helloWorld;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloWorldController {

	@RequestMapping(value="/")
	private String hello() {
		return "index.html";
	}

	@RequestMapping(value="/stopwatch")
	private String stopwatch() {
		return "stopwatch.html";
	}

}
